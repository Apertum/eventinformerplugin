/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.apertum.event_informer;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Authenticator;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.PasswordAuthentication;
import java.net.Proxy;
import java.net.Socket;
import java.net.URL;
import java.nio.charset.Charset;
import ru.apertum.qsystem.client.QProperties;
import ru.apertum.qsystem.common.GsonPool;
import ru.apertum.qsystem.common.QConfig;
import ru.apertum.qsystem.common.QLog;
import ru.apertum.qsystem.common.cmd.JsonRPC20;
import ru.apertum.qsystem.common.exceptions.QException;
import ru.apertum.qsystem.common.model.INetProperty;

import static ru.apertum.qsystem.common.QLog.log;

/**
 * @author Egorov-EV
 */
public class Network {

    public static class BasicAuthenticator extends Authenticator {

        final String USERNAME;// = "web-to-crm5";
        final String PASSWORD;// = "555";

        public BasicAuthenticator(String USERNAME, String PASSWORD) {
            this.USERNAME = USERNAME;
            this.PASSWORD = PASSWORD;
        }

        @Override
        public PasswordAuthentication getPasswordAuthentication() {
            System.out.println("Password requested " + getRequestingHost() + " for scheme " + getRequestingScheme() + " u/p = " + USERNAME + "/" + PASSWORD);
            return new PasswordAuthentication(USERNAME, PASSWORD.toCharArray());
        }
    }

    // HTTP POST request
    private static String sendPost(String url, String outputData) throws Exception {
        QLog.l().logger().trace("HTTP POST request on \"" + url + "\n" + outputData);
        final URL obj = new URL(url);
        final HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        //add reuqest header
        con.setRequestMethod("POST");
        con.setRequestProperty("User-Agent", "QSystem");
        con.setRequestProperty("Content-Type", "text/json; charset=UTF-8");
        con.setRequestProperty("Accept", "text/json");
        con.setRequestProperty("Accept-Charset", "utf-8");

        // Send post request
        con.setDoOutput(true);
        con.setDoInput(true);

        try (BufferedWriter out = new BufferedWriter(new OutputStreamWriter(con.getOutputStream(), "UTF8"))) {
            out.append(outputData);
            out.flush();
        }

        if (con.getResponseCode() != 200) {
            QLog.l().logger().error("HTTP response code = " + con.getResponseCode());
            throw new QException("no_connect_to_server");
        }

        final StringBuffer response;
        try (BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF-8"))) {
            String inputLine;
            response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
        }

        //result
        final String res = response.toString();
        response.setLength(0);
        QLog.l().logger().trace("HTTP response:\n" + res);
        return res;
    }

    // HTTP GET request
    private static String sendGet(String url, String outputData) throws Exception {
        QLog.l().logger().trace("HTTP GET request  on " + url);
        final URL obj = new URL(url);
        final HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        //add reuqest header
        con.setRequestMethod("GET");
        con.setRequestProperty("User-Agent", "QSystem");

        if (con.getResponseCode() != 200) {
            QLog.l().logger().error("HTTP response code = " + con.getResponseCode());
            throw new QException("no_connect_to_server");
        }
        final StringBuffer response;
        try (BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF-8"))) {
            String inputLine;
            response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
        }

        //result
        final String res = response.toString();
        response.setLength(0);
        QLog.l().logger().trace("HTTP response:\n" + res);
        return res;
    }

    public static byte[] readInputStream(InputStream stream) throws IOException {
        final byte[] result;
        final DataInputStream dis = new DataInputStream(stream);
        result = new byte[stream.available()];
        dis.readFully(result);
        return result;
    }

    synchronized public static Long sendRpc(String url, String user, String password, JsonRPC20 cmd) {
        final String message;
        Gson gson = GsonPool.getInstance().borrowGson();
        try {
            message = gson.toJson(cmd);
        } finally {
            GsonPool.getInstance().returnGson(gson);
        }
        System.out.println(message);
        final String data;

        Authenticator.setDefault(new BasicAuthenticator(user, password));

        try {
            data = sendPost(url, message);
            //data = sendGet(url, message);
        } catch (Exception ex) {
            return -1L;
        }

        System.out.println("Response:\n" + data);

        gson = GsonPool.getInstance().borrowGson();
        try {
            final RespCmd rpc = gson.fromJson(data, RespCmd.class);
            if (rpc == null) {
                throw new RuntimeException("error_on_server_no_get_response");
            }

            return rpc.getResult();
        } catch (JsonSyntaxException ex) {
            throw new RuntimeException("bad_response" + "\n" + ex.toString());
        } finally {
            GsonPool.getInstance().returnGson(gson);
        }

    }

    private static Proxy getProxy(Proxy.Type proxyType) {
        if (QConfig.cfg().getProxy() != null) {
            log().trace("Proxy.Type." + proxyType + ": " + QConfig.cfg().getProxy().host + ":" + QConfig.cfg().getProxy().port);
            return new Proxy(proxyType, new InetSocketAddress(QConfig.cfg().getProxy().host, QConfig.cfg().getProxy().port));
        }
        if (QProperties.get().getProperty("proxy", "hostname") != null && QProperties.get().getProperty("proxy", "port") != null) {
            log().trace("Proxy.Type." + proxyType + ": " + QProperties.get().getProperty("proxy", "hostname").getValue()
                    + ":" + QProperties.get().getProperty("proxy", "port").getValueAsInt());
            return new Proxy(proxyType, new InetSocketAddress(QProperties.get().getProperty("proxy", "hostname").getValue(),
                    QProperties.get().getProperty("proxy", "port").getValueAsInt()));
        } else {
            return null;
        }
    }


    /**
     * Отсылаем команду и получаем ответ. По TCP.
     *
     * @param netProperty сеть.
     * @param message     данные.
     * @throws QException упало.
     */
    @SuppressWarnings("squid:S3776")
    public static synchronized void sendData(INetProperty netProperty, String message, Charset charset) throws QException {
        final Proxy proxy = getProxy(Proxy.Type.SOCKS);

        log().trace("EventInformerPlugin. Message \"" + message + "\" on " + netProperty.getAddress().getHostAddress() + ":" + netProperty.getPort() + "#\n" + message);
        try (final Socket socket = proxy == null ? (QConfig.cfg().doNotUseProxy() ? new Socket(Proxy.NO_PROXY) : new Socket()) : new Socket(proxy)) {
            log().trace("EventInformerPlugin. Socket was created.");
            socket.connect(new InetSocketAddress(netProperty.getAddress(), netProperty.getPort()), 3000);
            final byte[] data = message.getBytes(charset);
            socket.getOutputStream().write(data);
            socket.getOutputStream().flush();
            log().trace("EventInformerPlugin. Sending...");
        } catch (Exception ex) {
            throw new QException("EventInformerPlugin. Error plugin. Data wasnt sent.", ex);
        }
    }

}
