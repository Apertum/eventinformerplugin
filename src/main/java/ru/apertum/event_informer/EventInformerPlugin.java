/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.apertum.event_informer;

import com.google.gson.Gson;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.charset.Charset;
import ru.apertum.qsystem.common.CustomerState;
import ru.apertum.qsystem.common.GsonPool;
import ru.apertum.qsystem.common.QLog;
import ru.apertum.qsystem.common.cmd.CmdParams;
import ru.apertum.qsystem.common.cmd.JsonRPC20;
import ru.apertum.qsystem.common.exceptions.QException;
import ru.apertum.qsystem.common.exceptions.ServerException;
import ru.apertum.qsystem.common.model.INetProperty;
import ru.apertum.qsystem.common.model.QCustomer;
import ru.apertum.qsystem.extra.IChangeCustomerStateEvent;
import ru.apertum.qsystem.server.ServerProps;
import ru.apertum.qsystem.server.model.QProperty;

/**
 * @author Egorov-EV
 */
public class EventInformerPlugin implements IChangeCustomerStateEvent {

    final static private String SERVICE = "EventInformer";
    final static private String SECTION = "EVENT_INFORMER";
    final static private String IP = "ip";
    final static private String PORT = "port";
    final static private String EVENTS = "events";
    final static private String END_MSG_CRLF = "crlf";
    final static private String END_MSG_TXT = "finalize";
    final static private String CHARSET = "charset";
    final static private String DATATYPE = "datatype";


    final private QProperty ipProp = ServerProps.getInstance().getProperty(SECTION, IP);
    final private QProperty portProp = ServerProps.getInstance().getProperty(SECTION, PORT);
    final private QProperty eventsProp = ServerProps.getInstance().getProperty(SECTION, EVENTS);
    final private QProperty endMsgCrlfProp = ServerProps.getInstance().getProperty(SECTION, END_MSG_CRLF);
    final private QProperty endMsgTxtProp = ServerProps.getInstance().getProperty(SECTION, END_MSG_TXT);
    final private QProperty charsetProp = ServerProps.getInstance().getProperty(SECTION, CHARSET);
    final private QProperty datatypeProp = ServerProps.getInstance().getProperty(SECTION, DATATYPE);

    final private CmdParams params = new CmdParams();
    final private JsonRPC20 cmd = new JsonRPC20(SERVICE, params);

    @Override
    public String getDescription() {
        return "EventInformerPlugin. Plugin for informing third system about events with clients.";
    }

    @Override
    public long getUID() {
        return 92037556127L;
    }

    /**
     * Когда не нужно менять состояние, но плагин дернуть надо. Например при повторном вызове.
     */
    @Override
    public void change(QCustomer qc, CustomerState cs, Long l) {
        if (ipProp == null || portProp == null || eventsProp == null) {
            QLog.l().logger().error("EventInformerPlugin. No parameters. Must be present parameters:\n\"EVENT_INFORMER.ip\"=" + ipProp
                    + "\n\"EVENT_INFORMER.port\"=" + portProp + "\n\"EVENT_INFORMER.events\"=" + eventsProp);
            return;
        }
        if (ipProp.getValue() == null || portProp.getValue() == null || eventsProp.getValue() == null) {
            QLog.l().logger().error("EventInformerPlugin. Bad parameters. \n ip=" + ipProp.getValue() + "\n port=" + portProp.getValue() + "\n events=" + eventsProp.getValue());
            return;
        }

        final INetProperty netProperty = new INetProperty() {
            @Override
            public Integer getPort() {
                return portProp.getValueAsInt();
            }

            @Override
            public InetAddress getAddress() {
                try {
                    return InetAddress.getByName(ipProp.getValue());
                } catch (UnknownHostException e) {
                    throw new ServerException("EventInformerPlugin. Ip address \"" + ipProp.getValue() + "\" is not correct.", e);
                }
            }

            @Override
            public String toString() {
                return getAddress().getHostName() + ":" + getPort();
            }
        };

        new Thread(() -> eventsProp.getValueAsIntArray("\\D+").stream().filter(state -> state.equals(cs.ordinal())).forEach(state -> {

            final String message;
            if (datatypeProp != null && datatypeProp.getValue() != null) {
                switch (datatypeProp.getValue()) {
                    case "json": {
                        final Gson gson = GsonPool.getInstance().borrowGson();
                        try {
                            message = gson.toJson(qc);
                        } finally {
                            GsonPool.getInstance().returnGson(gson);
                        }
                        break;
                    }
                    default: {
                        message = qc.getId().toString();
                    }
                }
            } else {
                message = qc.getId().toString();
            }

            final String msg = message +
                    (endMsgTxtProp == null || endMsgTxtProp.getValue() == null ? "" : endMsgTxtProp.getValue()) +
                    (endMsgCrlfProp == null || endMsgCrlfProp.getValueAsBool() == null || !endMsgCrlfProp.getValueAsBool() ? "" : "\r\n");

            final Charset charset = (charsetProp == null || charsetProp.getValue() == null || !Charset.isSupported(charsetProp.getValue())) ?
                    Charset.forName("utf-8")
                    :
                    Charset.forName(charsetProp.getValue());
            try {
                Network.sendData(netProperty, msg, charset);
            } catch (QException ex) {
                QLog.l().logger().error("EventInformerPlugin. Message wasnt sent to " + netProperty, ex);
            }
        })).start();
    }

    @Override
    public void change(String userPoint, String customerPrefix, int customerNumber, CustomerState cs) {
        //когда не нужно менять состояние, но плагин дернуть надо. Например при повторном вызове.
        throw new UnsupportedOperationException("EventInformerPlugin. Not supported yet.");
    }

}
