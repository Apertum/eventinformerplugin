/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.apertum.event_informer;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URLDecoder;
import java.net.UnknownHostException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import javax.swing.UnsupportedLookAndFeelException;
import ru.apertum.qsystem.common.exceptions.ClientException;
import ru.apertum.qsystem.common.exceptions.QException;
import ru.apertum.qsystem.common.exceptions.ServerException;
import ru.apertum.qsystem.common.model.INetProperty;

/**
 * @author SBT-Egorov-EV
 */
public class FTest extends javax.swing.JFrame {

    public static byte[] readInputStream(InputStream stream) throws IOException {
        DataInputStream dis = new DataInputStream(stream);
        byte[] result = new byte[stream.available()];
        dis.readFully(result);
        return result;
    }

    private String readInputStrim(Socket socket) {
        try {
            final InputStream is = socket.getInputStream();
            // подождать пока хоть что-то приползет из сети, но не более 10 сек.
            int i = 0;
            while (is.available() == 0 && i < 100) {
                Thread.sleep(100);//бля
                i++;
            }

            final StringBuilder sb = new StringBuilder(new String(readInputStream(is), StandardCharsets.UTF_8));
            while (is.available() != 0) {
                sb.append(new String(readInputStream(is), StandardCharsets.UTF_8));
                Thread.sleep(150);//бля
            }
            return URLDecoder.decode(sb.toString(), StandardCharsets.UTF_8.name());
        } catch (IOException ex) {
            throw new ServerException("Ошибка при чтении из входного потока: " + ex);
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
            throw new ServerException("Проблема со сном: " + ex);
        } catch (IllegalArgumentException ex) {
            throw new ServerException("Ошибка декодирования сетевого сообщения: " + ex);
        }
    }

    /**
     * Creates new form FTest
     */
    public FTest() {
        initComponents();

        new Thread(() -> {

            // привинтить сокет на порт
            try (final ServerSocket server = new ServerSocket(27026)) {
                taLog.append("Тестовый TCP сервер захватывает порт \"" + 27026 + "\".\n");
                //server.setSoTimeout(500);
                boolean exit = false;
                // слушаем порт
                while (!Thread.interrupted()) {
                    Socket socket = server.accept();
                    // из сокета клиента берём поток входящих данных
                    final String data = readInputStrim(socket);
                    System.out.println("OBTAIN DATA: \"" + data + "\".");
                    taLog.append("OBTAIN DATA: \"" + data + "\".\n\n");
                }// while

                taLog.append("Закрываем серверный сокет.");
            } catch (Exception e) {
                System.err.println(e);
            }

        }).start();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel5 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        taLog = new javax.swing.JTextArea();
        jLabel4 = new javax.swing.JLabel();
        tfInputed = new javax.swing.JTextField();
        jPanel1 = new javax.swing.JPanel();
        btnSend = new javax.swing.JButton();
        tfIp = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        spPort = new javax.swing.JSpinner();

        jLabel5.setText("jLabel5");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Плагин для проверки отправленных данных.");

        taLog.setColumns(20);
        taLog.setFont(new java.awt.Font("Monospaced", 0, 12)); // NOI18N
        taLog.setRows(5);
        jScrollPane1.setViewportView(taLog);

        jLabel4.setText("Что ввели");

        tfInputed.setText("89039091212");

        jPanel1.setBorder(new javax.swing.border.MatteBorder(null));

        btnSend.setText("Отправить запрос");
        btnSend.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSendActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnSend)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnSend)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        tfIp.setText("127.0.0.1");

        jLabel6.setText("IP");

        jLabel7.setText("port");

        spPort.setModel(new javax.swing.SpinnerNumberModel(27026, 1, 64000, 1));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel6)
                    .addComponent(jLabel7)
                    .addComponent(jLabel4))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(spPort, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tfInputed, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tfIp, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(631, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tfIp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(spPort, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tfInputed, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 368, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnSendActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSendActionPerformed
        System.out.println("Go...");
        taLog.append("Send data " + tfInputed.getText() + "...\n");
        //{"jsonrpc": "2.0", "method": "GetServiceForInputData", "params": {"input_data": "1234"}, "id": 1}
        /*
        CmdParams params = new CmdParams();
        params.textData = tfInputed.getText();

        final JsonRPC20 cmd = new JsonRPC20("GetServiceForInputData", params);


        final String message;
        Gson gson = GsonPool.getInstance().borrowGson();
        try {
            message = gson.toJson(cmd);
        } finally {
            GsonPool.getInstance().returnGson(gson);
        }
        System.out.println(">> " + message);
        taLog.append(message + "\n");
         */
        final INetProperty netProperty = new INetProperty() {
            @Override
            public Integer getPort() {
                return (Integer) spPort.getValue();
            }

            @Override
            public InetAddress getAddress() {
                try {
                    return InetAddress.getByName(tfIp.getText());
                } catch (UnknownHostException e) {
                    throw new ClientException("EventInformerPlugin. Ip address \"" + tfIp.getText() + "\" is not correct.", e);
                }
            }

            @Override
            public String toString() {
                return getAddress().getHostName() + ":" + getPort();
            }
        };

        try {
            Network.sendData(netProperty, tfInputed.getText(), Charset.forName("utf-8"));
        } catch (QException ex) {
            taLog.append("Result: ERROR " + ex.getLocalizedMessage() + " \n" + ex + "\n\n");
            throw new ServerException("EventInformerPlugin. Error sending.", ex);
        }

        taLog.append("Result: OK\n");
        /*
        RespCmd res = new RespCmd();
        res.setResult(123123123L);

        gson = GsonPool.getInstance().borrowGson();
        try {
            System.out.println("example: " + gson.toJson(res));
        } finally {
            GsonPool.getInstance().returnGson(gson);
        }
         */

    }//GEN-LAST:event_btnSendActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                //System.out.println(info.getName());
                /*Metal Nimbus CDE/Motif Windows   Windows Classic  */
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
        }

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() -> {
            final FTest form = new FTest();
            form.setLocationRelativeTo(null);
            form.setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnSend;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSpinner spPort;
    private javax.swing.JTextArea taLog;
    private javax.swing.JTextField tfInputed;
    private javax.swing.JTextField tfIp;
    // End of variables declaration//GEN-END:variables
}
