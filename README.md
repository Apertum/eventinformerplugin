# EventInformerPlugin

Плагин для рассылки оповещения для третьих систем о событиях с кастомерами в QSystem.

**Причины**

Иногда в другой системе нужно получить сигнал о событиях в очереди. Для этого сделан этот плагин.
Когда оператор управляет очередью, этот плагин высылает сообщение по сети. Все заинтересованные системы слушают сеть, получают сообщение.

**Общая идея решения** 

В QSystem заводятся настройки в админке. Создается группа  EVENT_INFORMER. В нее добавляется три параметра: ip, port, events.
На этот адрес и порт по TCP будет отправлен ID посетителя если событие присутствует в списке events. События указываются как цыфры через любой разделитель.

    
     * 0 удален по неявке. STATE_DEAD
    
     * 1 стоит и ждет в очереди. STATE_WAIT
    
     * 2 стоит и ждет в очереди после того, как отлежался в отложенных положенное время и автоматически отправился в прежнюю очередь с повышенным приоритетом. STATE_WAIT_AFTER_POSTPONED
    
     * 3 Кастомер был опять поставлен в очередь т.к. услуга комплекстая и ждет с номером. STATE_WAIT_COMPLEX_SERVICE
    
     * 4 пригласили. STATE_INVITED
    
     * 5 пригласили повторно в цепочке обработки. т.е. клиент вызван к оператору не первый раз а после редиректа или отложенности. STATE_INVITED_SECONDARY
    
     * 6 отправили в другую очередь, идет как бы по редиректу в верх. Стоит ждет к новой услуге. STATE_REDIRECT
    
     * 7 начали с ним работать. STATE_WORK
    
     * 8 начали с ним работать повторно в цепочке обработки. STATE_WORK_SECONDARY
    
     * 9 состояние когда кастомер возвращается к прежней услуге после редиректа, по редиректу в низ. Стоит ждет к старой услуге. STATE_BACK
    
     * 10 с кастомером закончили работать и он идет домой.  STATE_FINISH
    
     * 11 с кастомером закончили работать и поместили в отложенные. домой не идет, сидит ждет покуда не вызовут. STATE_POSTPONED
     

**Дополнительные настройки**

EVENT_INFORMER.crlf = true/false - завершать посылку байтами CRLF или нет. Необязательный параметр. По умолчанию не добавляется.

EVENT_INFORMER.finalize = <text> - добавлять в конец посылки. Необязательный параметр. По умолчанию не добавляется.

EVENT_INFORMER.charset = название кодировки посылки. Необязательный параметр. По умолчанию не добавляется. UTF-8, cp1251, windows-1251, US-ASCII, KOI8-R, ...

EVENT_INFORMER.datatype = [json] - вариативность рассылаемой информации. Пока только json. Необязательный параметр. По умолчанию не добавляется.


